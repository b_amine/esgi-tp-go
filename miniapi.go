package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"time"
)

func timeHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		currTime := time.Now()
		hh := currTime.Hour()
		mm := currTime.Minute()
		fmt.Fprintf(w, "%02dh%02d", hh, mm)
	} else {
		fmt.Fprintf(w, "Only GET allowed")
	}
}

func entriesHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		entries, err := os.ReadFile("./entries.data")
		if err == nil {
			fmt.Fprintf(w, string(entries))
		}
	} else {
		fmt.Fprintf(w, "Only GET allowed")
	}
}

func helloHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		if len(req.PostForm) != 2 {
			fmt.Println("Incorrect number of parameters")
			fmt.Fprintln(w, "Incorrect number of parameters")
			return
		}
		if val, ok := req.PostForm["author"]; !ok {
			fmt.Fprintln(w, "No author !")
			fmt.Print(val)
			return
		}
		if val, ok := req.PostForm["entry"]; !ok {
			fmt.Fprintln(w, "No entry !")
			fmt.Print(val)
			return
		}
		author := req.PostForm["author"][0]
		entry := req.PostForm["entry"][0]

		saveFile, err := os.OpenFile("./entries.data", os.O_APPEND|os.O_RDWR|os.O_CREATE, 0755)
		defer saveFile.Close()

		w2 := bufio.NewWriter(saveFile)
		if err == nil {
			fmt.Fprintf(w2, "%s\n", entry)
		}
		w2.Flush()

		fmt.Fprintf(w, author+": "+entry)
	} else {
		fmt.Fprintf(w, "Only POST allowed")
	}
}

func main() {
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/hello", helloHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":9000", nil)
}
